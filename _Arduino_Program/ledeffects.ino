// Main LED PWM controller
void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(red_light_pin, red_light_value);
  analogWrite(green_light_pin, green_light_value);
  analogWrite(blue_light_pin, blue_light_value);
}

// Green up and flash RGB
void ledStartUp(){
    // Make LEDs to show startup
    for (int i = 0; i <= 255; i++)
    {
      RGB_color(0, i, 0); // Green
      delay(5);
    }
    RGB_color(0, 0, 0); // Make LED off
    delay(100);
    RGB_color(255, 255, 255); // Make LED on agian
    delay(100);
    RGB_color(0, 0, 0); // Make LED off after 'Setup flash'
}

// Check R - G - B LEDs with given speed
void rgbTest(int speedVar)
{
  for (int i = 0; i < 255; i++){
  RGB_color(i, 0, 0); // Red
  delay(speedVar); 
  }
  RGB_color(0, 0, 0);
  
  for (int i = 0; i < 255; i++){
  RGB_color(0, i, 0); // Red
  delay(speedVar); 
  }
  RGB_color(0, 0, 0);

   for (int i = 0; i < 255; i++){
  RGB_color(0, 0, i); // Red
  delay(speedVar); 
  }
  RGB_color(0, 0, 0);
}
