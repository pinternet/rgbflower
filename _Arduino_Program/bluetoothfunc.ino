void connectionHandler()
{
  // Check state change
  if(digitalRead(bluetooth_conn) and not pre_connection)
  {
    // Flash for Bluetooth mode
    if (!man_mob_mode)
    {
      for (int i = 0; i < 5; i++){
        RGB_color(0, 0, 0);
        delay(100);
        RGB_color(0, 255, 0);
        delay(100);
      }
      // Swith to bluetooth mode
      man_mob_mode = 1;
    }
  }
  pre_connection = digitalRead(bluetooth_conn);
}

void BluetoothBytesTest()
{
  // check if data is available
  if (Serial.available() > 0) {
    // read the incoming bytes:
    int rlen = Serial.readBytes(serialBuf, BUFFER_SIZE);

    if(rlen == 5)
    {
      bluetooth_mode = serialBuf[0];  // Mode from the bluetooth control
      bluetooth_red = serialBuf[1];   // R
      bluetooth_green = serialBuf[2]; // G
      bluetooth_blue = serialBuf[3];  // B
      bluetooth_speed = serialBuf[4]; // Speed
    }
  }
}
void BluetoothTest()
{
   /* 
    * This is a test funtion against bluetooth switch application, which change the colour to R and B
   */ 
    if(Serial.available()>0)
   {     
      char data= Serial.read(); // reading the data received from the bluetooth module
      switch(data)
      {
        case '0': RGB_color(255,0,0);break; // when a is pressed on the app on your smart phone
        case '1': RGB_color(0,255,0);break; // when d is pressed on the app on your smart phone
        default : break;
      }
      Serial.println(data);
   }
   delay(50);
}
