//******************************************* ADDITIONAL FUNCTIONS ***************************************
// Functions
void autoTimer(){
  if(bluetooth_switch_off == 0)
  {
    // Switch LEDs off
    bluetooth_mode = 0; // Mode from the bluetooth control
    bluetooth_red = 0;
    bluetooth_green = 0;
    bluetooth_blue = 0;
    bluetooth_speed = 0; 
    man_mob_mode = 1;  

    // Switch off bluetooth
    Serial.end(); 
  }
}
