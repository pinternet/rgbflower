//****************************************** SPECIAL FUNCTIONS *********************************************

// Set parameters for InOuts
void setup(){
  /*
   * Setting up pinouts
   */
   
  // RGB LED D6, D5, D3
  pinMode(red_light_pin, OUTPUT);
  pinMode(green_light_pin, OUTPUT);
  pinMode(blue_light_pin, OUTPUT);
  
 /*
  // Set timer interrupt
  noInterrupts();                       // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;
  // Set timer1_counter to the correct value for our interrupt interval
  //timer1_counter = 64911;   // preload timer 65536-16MHz/256/100Hz
  //timer1_counter = 64286;   // preload timer 65536-16MHz/256/50Hz
  timer1_counter = 34286;   // preload timer 65536-16MHz/256/2Hz
  TCNT1 = timer1_counter;   // preload timer
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
  */
/*
  // Manual - Mobile button D2
  attachInterrupt(digitalPinToInterrupt(man_mob_button),buttonPressed1,RISING); 

  // Bluetooth connection feedback
  pinMode(bluetooth_conn, INPUT);
  
  // Bluetooth bauderate
   Serial.begin(9600);
   delay(1000);
   while (!Serial) {
       RGB_color(255, 0, 0); // wait for serial port to connect. Needed for native USB port only
       delay(100);
   }
   RGB_color(0, 255, 0);
   // Serial.println("ASCII Table ~ Character Map");
   delay(1000);
   RGB_color(0, 0, 0);*/
}
