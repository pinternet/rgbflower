// Includes
# include <SoftwareSerial.h>
SoftwareSerial MyBlue(0,1); // RX | TX 
// 
// Define InOuts
int red_light_pin = 6;   
int green_light_pin = 5;   
int blue_light_pin = 3;  
int man_mob_button = 2;

// Global variables
bool man_mob_mode = 1; // Manual - 0, Mobile - 1
int bluetooth_mode = 0; // Mode from the bluetooth control
int bluetooth_red = 0;
int bluetooth_green = 0;
int bluetooth_blue = 0;
int bluetooth_speed = 0;
int timer1_counter;
int my_timer = 0;
int flag = 0; 
int pre_time;
int testem = 0;


//****************************************** SPECIAL FUNCTIONS *********************************************
// Set parameters for InOuts
void setup() {
  /*
   * Setting up pinouts
   */
   
  // RGB LED D6, D5, D3
  pinMode(red_light_pin, OUTPUT);
  pinMode(green_light_pin, OUTPUT);
  pinMode(blue_light_pin, OUTPUT);

  // Set timer interrupt
  noInterrupts();                       // disable all interrupts
  
  TCCR1A = 0;
  TCCR1B = 0;

  // Set timer1_counter to the correct value for our interrupt interval
  //timer1_counter = 64911;   // preload timer 65536-16MHz/256/100Hz
  //timer1_counter = 64286;   // preload timer 65536-16MHz/256/50Hz
  timer1_counter = 34286;   // preload timer 65536-16MHz/256/2Hz
  
  TCNT1 = timer1_counter;   // preload timer
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
  interrupts();                         // enable all interrupts  

  // Manual - Mobile button D2
  attachInterrupt(digitalPinToInterrupt(man_mob_button),buttonPressed1,RISING); 

  // Bluetooth bauderate
  Serial.begin(9600); 
  MyBlue.begin(9600); 
  Serial.println("Ready to connect\nDefualt password is 1234 or 000"); 
}

void buttonPressed1() //HW interrupt         
{ 
  /*                  
   * Swith between manual flash and mobile selection with HW interrupt
   */
  // change mode
  if (man_mob_mode)
  {
    man_mob_mode = 0;
  }
  else
  {
    man_mob_mode = 1; 
  }

  // reset leds
  // RGB_color(0,0,0);

  // TODO: reset variables
  my_timer = 0;
}

ISR(TIMER1_OVF_vect)// interrupt service routine for overflow
{
  TCNT1 = timer1_counter;   // preload timer
  my_timer = my_timer + 4;  //Turns LED ON and OFF
}

//****************************************** MAIN LOOP ****************************************************
// Main Cycle
void loop() {

  if (man_mob_mode)
  {
    /* Bluetooth communication mode
     * Mode:
     * 0 - waiting for command
     * 1 - Manual mode (swithch to manual mode)
     * 2 - Test mode (R_G_B led and communication test
     * 3 - Normal R - G - B mixing with pormeter
     * 4 - Color mix with brightness
     * 10 - 19 Light play
     * 20 - Microphone control
     * 21 - Loudspeaker control
     * 50 - Bluetooth parameterisation
     */
    BluetoothTest(); //test bluetooth mobile switch
  }
  else
  {
    if (pre_time != my_timer)
    {
      testem = testem + 51;
      RGB_color(testem, 0, 0); // Red
    }
    pre_time = my_timer;
    // rgbTest(10);    //test manual mode
  }
}


//******************************************* ADDITIONAL FUNCTIONS ***************************************
// Functions

void BluetoothTest()
{
   /* 
    * This is a test funtion against bluetooth switch application, which change the colour to R and B
   */ 
    if(Serial.available()>0)
   {     
      char data= Serial.read(); // reading the data received from the bluetooth module
      switch(data)
      {
        case '0': RGB_color(255,0,0);break; // when a is pressed on the app on your smart phone
        case '1': RGB_color(0,255,0);break; // when d is pressed on the app on your smart phone
        default : break;
      }
      Serial.println(data);
   }
   delay(50);
}
void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(red_light_pin, red_light_value);
  analogWrite(green_light_pin, green_light_value);
  analogWrite(blue_light_pin, blue_light_value);
}

void rgbTest(int speedVar)
{
  for (int i = 0; i < 255; i++){
  RGB_color(i, 0, 0); // Red
  delay(speedVar); 
  }
  RGB_color(0, 0, 0);
  
  for (int i = 0; i < 255; i++){
  RGB_color(0, i, 0); // Red
  delay(speedVar); 
  }
  RGB_color(0, 0, 0);

   for (int i = 0; i < 255; i++){
  RGB_color(0, 0, i); // Red
  delay(speedVar); 
  }
  RGB_color(0, 0, 0);
}
